<!DOCTYPE html>
<html lang="sv">
	<head>
		<?php include './include/head'; ?>	
		<title>Spela Roll</title>
	</head>
	<body>
		<?php include './include/nav.htm'; ?>
		<header class="title">
			<h1>Spela Roll</h1>	
		</header>
		<aside>
		<article>
			<header>
				<h2>Styrelse</h2>	
			</header>
			<p>
				<table>
					<tbody>
						<tr>
							<td>Ordförande:</td>
							<td>Stefan Björk-Olsén</td>
						</tr>
						<tr>
							<td>Sekreterare:</td>
							<td>Kristian Nilsson</td>
						</tr>
						<tr>
							<td>Kassör:</td>
							<td><address><a href="mailto:swe_jonny@hotmail.com">Jonny Svensson</a></address></td>
						</tr>
						<tr>
							<td>Revisor:</td>
							<td>Mikael Bylund</td>
						</tr>				
					</tbody>
				</table>
			</p>
			</article>
			<article>
				<header>
					<h2>Kontakt</h2>	
				</header>
			<p>
				<address>
					Spela Roll<br>
					Tjärbyvägen 18<br>
					31235 Laholm
				</address>
			</p>
		</aside>
		<section class="main">
			<article>
				<figure id="arkhamhorror">
					<img src="./img/AH-Stefan.jpg" alt="Arkham Horror. Bild tagen av Stefan Björk-Olsén.">
					<figcaption>Arkham Horror. Bild tagen av Stefan Björk-Olsén.</figcaption>
				</figure>
				
				<header>
					<h2>Föreningen</h2>
				</header>			
			
				<p>
					<ul>
						<li>Brädspel, kortspel, figurspel, rollspel.</li>
						<li>De flesta speltillfällen är öppna även för icke-medlemmar (i mån av plats).</li>
					</ul>
				</p>
			
			</article>
			<article>
				<header>
					<h2>Dokument</h2>	
				</header>
				<p>
					<ul>
						<li>Protokoll</li>
						<li>Bokföring</li>
					</ul>
				</p>
			</article>
		</section>
<iframe src="https://docs.google.com/spreadsheets/d/14SiYDE4R_b-09GsgiER4i6t1wCIj2Ef9CSxbeirnZG0/pubchart?oid=1406734986&amp;format=image"></iframe>		<?php include './include/footer'; ?>
	</body>
</html>
