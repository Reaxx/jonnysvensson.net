<!DOCTYPE html>
<html lang="sv">

<head>
	<?php include './include/head'; ?>
	<title>Jonny Svensson</title>
</head>

<body>

	<?php include './include/nav.htm'; ?>

	<header class="title">
		<h1>Jonny Svensson</h1>
	</header>

	<aside id="me">
		<article>
			<p class="italic">
				För stunden ägnar jag mina nätter åt att köra tidningsbil och lyssna på podcasts, men söker mig vidare mot arbete
				inom systemutveckling och tar därför tacksamt tacksamt emot tips och kommunikationer om arbeten i eller omkring
				Halland rörande framförallt C#/.net och/eller PHP.
			</p>

			<header>
				<h1>Om mig</h1>
			</header>

			<p>
				Jag har en något krokig bana bakom mig med ett stort intresse för problemlösning och teknik som resluterade i en en
				tekn. kand från Halmstad Högskola
				medan jag istället framförallt arbetat praktiskt inom t.ex. bygg och industri.
			</p>
			<p>
				Mitt generella teknikintresse grundlades tidigt och jag kan bla. minnas hur jag som barn plockade sönder
				radioapparater och försökte ta tillvara komponenterna, även om mitt intresse ganska
				snart kom att fokusera på datorer (och datorspel).
			</p>
			<p>
				När det kommer till programmering så satt jag tidigt och gjorde extremt enkla hemsidor, och insåg nog inte att man
				kunde göra så mycket mer förrens omkring millenieskiftet då
				jag gick enkel sommarkurs där jag som tonåring fick lära mig grunderna i C++. Kunskaper som jag sen byggde vidare
				på genom att i mångt och mycket testa mig fram och försöka hitta användbara
				delar av tekniska texter jag egentligen inte förstod mig på, kunskap som jag därefter på egen hand överförde till
				PHP då jag insåg hur mycket lättilgängligare koden då blev som därefter blev
				mitt huvudsakliga (och ganska länge egentlige nenda) programmerinsspråk.
			</p>
			<!--  
				I takt med att jag blev äldre så gled mitt intresse tillbaks mot hårdvara oc..
			</p>
			 -->
			<p>
				Jag ser mig därför i praktiken i mångt och mycket självlärd men insåg samtidigt att min kod var allt annat än
				modern och läsbar, så när jag på senare år fick möjlighet att läsa programmeringskurser på högre
				och rimligare nivåer var jag inte sen att tacka ja. Studier som resluterat i att jag nu även kodar i C#(/.net) och
				klarade Microsofts exam 483: Programming in C#.
			</p>
			<p>
				<h2>Projekt</h2>
				Jag har alltid ett antal projekt igång som jag arbetar på i mån av tid och intresse, det primära just nu är vid
				sidan av en större webbsida handlar framförallt om att jag håller på att bygga
				en ny webbsida åt en av mina föreningar, chat-rollspelet Simultima, samtidigt som jag håller på att porta över mina
				äldre webbsidor och PHP-projekt till att använda Fat-Free Framework och bygga
				om min kod mer i linje med OOP, men har också en hel del små c# projekt som jag skriver på i mån av tid.
			</p>
			<p>
				<h2>
					Övrigt
				</h2>

				Vid sidan av IT-intresset så är jag också väldigt förtjust i både spelande i de flesta former
				och är blandannat aktiv som deltagare och i vissa fall styrelsemdlem i flera brädspels (<a href="http://spelaroll.eu"
				 target="_blank" title="Spela Roll">Spela Roll</a>,
				<a href="http://www.unitedgamers.se/" target="_blank" title="Halsmtad United Gamers">HUG</a>) och
				rollspelsföreninger (<a href="http://www.simultima.se/" target="_blank" title="Simultima">Simultima</a>).
			</p>
		</article>
	</aside>

	<section id="fact">
		<article>
			<p>
				<b>Namn:</b> Jonny Svensson<br>
				<b>Telefon:</b> 076-777 36 74<br>
				<b>Email:</b> <a href="mailto:swe_jonny@hotmail.com">swe_jonny@hotmail.com</a> <br>
				<b>Boende:</b> Laholm, Halland<br>
				<a href="https://www.facebook.com/P.Jonny.Svensson" target="_blank" title="Facebook">Facebook</a><br>
				<a href="https://www.linkedin.com/in/jonny-svensson-74073638/" target="_blank" title="LinkedIn">LinkedIn</a><br>
			</p>
		</article>

		<article>
			<header>
				<h1>Projekt</h1>
			</header>
			<details>
				<summary>SpelaRoll.eu</summary>
				<ul>
					<li><a href="http://www.spelaroll.eu" target="_blank">SpelaRoll.eu</a>, Webbsida jag i perioder aktivt utvecklar
						för min
						brädspelsföreningen (se länken), framförallt skriven i småkass PHP med lite JS funktioner men ska bättras på så
						snart det finns tid för det.</li>
					<li><a href="https://bitbucket.org/Reaxx/spelaroll" title="BitBucket repo" target="_blank">BitBucket Repo för
							spelaroll</a></li>
				</ul>
			</details>
			<details>
				<summary>JonnySvensson.net</summary>
				<ul>
					<li><a href="http://www.jonnysvensson.net" target="_blank">JonnySvensson.net</a>, sidan du antagligen läser det
						här på, och min personliga webbsida. Mestadels enkel PHP med lite hjälp av JS.
						Ligger tyvärr ofta orörd och ouppdaterad när jag inte kommer på något intressant att göra med den.</li>
					<li><a href="https://bitbucket.org/Reaxx/spelaroll" title="BitBucket repo" target="_blank">BitBucket Repo för
							JonnySvensson.net</a></li>
				</ul>
			</details>
			<details>
				<summary>SW_Destiny</summary>
				<ul>
					<li>Jag och några vänner började ungefär samtidigt spela
						<a href="https://www.fantasyflightgames.com/en/products/star-wars-destiny/" title="Fantasy Flight Games" target="_blank">Star
							Wars Destiny</a>
						och intressera oss för C#/.NET, så vi drog igång ett litet projekt för att lära oss språket och samtidigt hantera
						kort och kortlistor.
						Jag agerar primärt back-end utvecklare medan de andra primärt ägnar sig åt GUI för olika operativsysttem.</li>
					<li><a href="https://bitbucket.org/Reaxx/sw_destiny" title="BitBucket repo" target="_blank">BitBucket Repo för
							sw_destiny</a></li>
				</ul>
			</details>

			<details>
				<summary>Repos</summary>
				<ul>
					<li><a href="https://bitbucket.org/Reaxx/" target="_blank">Bitbucket</a></li>
				</ul>
			</details>
		</article>

		<article>
			<header>
				<h1>Arbete</h1>
			</header>
			<details>
				<summary>Tidningsdistrubitör</summary>
				<ul>
					<li><a href="http://www.vtd.se/" target="_blank" title="VTD">VTD</a> (2018-2019)</li>
					<li></li>
				</ul>
			</details>
			<details>
				<summary>Systemutvecklare</summary>
				<ul>
					<li><a href="http://konsultlink.com/" target="_blank" title="KonsulLink">KonsultLink</a> (2017-2018)</li>
					<li>Praktik där jag huvusakligen arbetade i PHP i FatFree Framework.</li>
				</ul>
			</details>

			<details>
				<summary>Labbhandledare</summary>
				<ul>
					<li><a href="http://www.hh.se/" target="_blank" title="Högskolan i Halmstad">Högskolan i Halmstad</a> (2012)</li>
					<li>Totalt ett hundratal elever och 80 timmar spritt över flera av <a href="" title="IT-Forensik och Informationssäkerhet">ITFs</a>
						profilkurser.</li>
				</ul>
			</details>
			<details>
				<summary>Internetcafé</summary>
				<ul>
					<li>Web'N'Game Spot (2009)</li>
					<li>Allt från kassatjänst till drift av servrar.</li>
				</ul>
			</details>
			<details>
				<summary>Snickare</summary>
				<ul>
					<li><a href="http://www.pms.nu/pms/" target="_blank" title="PMS.nu">PMS Fastigheter i Mellby AB</a>
						(2007-2008,2013-2014)</li>
					<li>Okvalificerat arbete i familjeföretag.</li>
				</ul>
			</details>
			<details>
				<summary>Industriarbete</summary>
				<ul>
					<li>Presshalls- och Expanderingstekinker <a href="http://www.diabgroup.com/" title="DIAB Group" target="_blank">DIAB
							AB</a> (2006-2007)</li>
				</ul>
			</details>
		</article>

		<article>
			<header>
				<h1>Utbildning</h1>
			</header>
			<details>
				<summary>.Net Utvecklare</summary>
				<ul>
					<li>Lexicon i Väst (2017)</li>
					<li>Yrkesinriktad utbildning för .NET mjukvarutveckling (med fokus på C# och ASP.net). Totalt 4 månader "teori",
						och därefter 3 månader praktik.</li>
				</ul>
			</details>
			<details>
				<summary>IT-Forensik och informationssäkerhet</summary>
				<ul>
					<li><a href="">Högskolan i Halmstad</a> (2009-2012)</li>
					<li>180 HP</li>
				</ul>
			</details>
			<details>
				<summary>Fria högskolekurser</summary>
				<ul>
					<li>Blandannat Juridik och Öppen källkod.</li>
					<li>Totalt 27.5HP</li>
				</ul>
			</details>
			<details>
				<summary>Teknikprogrammet</summary>
				<ul>
					<li><a href="">Osbecksgymnasiet</a> (2003-2005)</li>
				</ul>
			</details>
		</article>

		<article>
			<header>
				<h1>Cert</h1>
			</header>
			<details>
				<summary>Exam 483: Programming in C#</summary>
				<ul>
					<li>Via Lexicon i Väst (2018)</li>
					<li><a href="https://www.youracclaim.com/badges/93cf63df-8602-4bbb-8456-c42793dbd294/linked_in_profile" target="_blank">Acclaim
							Badge</a></li>
				</ul>
		</article>

	</section>

	<footer>
		<address>
			Jonny Svensson <br>
			Tjärbyvägen 18 <br>
			31235 Laholm <br>
			076-777 36 74<br>
			<a href="mailto:swe_jonny@hotmail.com">swe_jonny@hotmail.com</a>
		</address>
		<small id="date">
			Senast uppdaterad: <time datetime="<?php echo date(" Y-m-d H:i", getlastmod()); ?>" pubdate>
				<?php echo date("Y-m-d H:i", getlastmod()); ?></time>
		</small>
	</footer>
</body>

</html>