<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="style.css">
		<script src="code.js"></script>
		<title>Minecraft</title>
	</head>
	<body>
		<?php include './include/nav.htm'; ?>
		<header class="title">
			<h1>Minecraft</h1>	
		</header>
		<article>
			<header>
				<h1></h1>
			</header>
			<p>
				Jag driver en liten Minecraft-server som framförallt är öppen för mig och mina bekanta, men så länge det inte sker någon vandalisering står den öppen för vem som helst. Servern är externt hostad och borde vara tämligen stabil, men då det rör sig om Minecraft så blir den då och då tämligen laggad, då är det bara att peta på mig så tar jag tag i problemet så fort jag hinner. 
			</p>
			<p>
				Värt att notera är att jag står för kostnaden av servern själv, och det kan i vissa fall skilja några dagar från att räkningen ska vara betalt och till att jag hinner betala, <a href="http://clients.fragnet.net/grouppay.php?hash=5efa5-2acc5-d2bde-d3460-cac59-d87f9-06" title="Group pay">donationer</a> till serverdrift tas tacksamt emot.
			</p>
			<ul>
				<li>Daglig backup</li>
				<li>IP: 46.253.196.86:25665</li>
			</ul>
		</article>
		<article>
			<header>
				<h1>Mods/Modpacks</h1>
			</header>
			<p>
				<ul>
					<li><a href="http://feed-the-beast.com/" title="Feed the Beast">Direwolf20 v 1.6.1 (Minecraft 1.7.10)</a></li>
				</ul>
			</p>
		</article>
		<article>
			<h2>Installation</h2>
			<details>
				<summary>
					Installation av Launcher, moddar och modpack.
				</summary>
				<ol>
					<li>Ladda ner och starta <a href="http://feed-the-beast.com/" title="Feed the Beast">FtB launchern</a> ("Download (exe)" för Windows, "Download (jar) för OSX och Linux". Vänta tills installationen är klar.</li>
					<li>Skapa en profil (med ditt vanliga Minecraft-konto).</li>
					<li>Välj Direwolf20 (v.1.6.1) Minecraft Version 1.7.10 i Modpack-listan.
					<li>Ändra rullistan från "Recommended" till 1.6.1</li>
					<li>Klicka på Launch. Vänta tills nerladdning och installation är klar och Minecraft startats. (Kan ta en stund innan det syns att något händer)</li>
					<li>Om nödvändigt upprepa steget ovan.
					<li>Tryck på Launch och vänta tills Minecraft har startat. (Kan ta en stund innan det syns att något händer)</li>
			</ol>
			</details>
			<details>
				<summary>
					Ansluta till servern (identisk med Vanilla).
				</summary>
				<ol>
					<li>Tryck på "Multiplayer" och därefter "Add Server"</li>
					<li>Ange det Server Name du vill ha, och ange 46.253.196.86:25665 som Server Address. Tryck på Done.</li>
					<li>Servern bör nu listas och en indikator visar om den är online och fungerande, dubbelklicka på namnet för att ansluta.</li>
				</ol>
			</details>
			<details>
				<summary>
					Felsökning.
				</summary>
				<ol>
					<li>Kontrollera att <a href="" title="Ska bli länk">senaste verisionen av Java</a> är installerd</li>
             <li>Kontakta <a href="" title="Ska bli länk">mig</a></li>
				</ol>
			</details>
		</article>
		<article>
			<p>
				<a href="http://clients.fragnet.net/grouppay.php?hash=5efa5-2acc5-d2bde-d3460-cac59-d87f9-06" title="Group pay">Group pay</a> <br>
				<fig><img src="http://46.253.196.86:25667/status.png" title="Minecraft server status" alt="Server down"></fig>
			</p>
		</article>
		
		<?php include './include/footer'; ?>
	</body>
</html>
