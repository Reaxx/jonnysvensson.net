<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="style.css">
		<title>Blood Bowl</title>
	</head>
	<body>
		<?php include './include/nav.htm'; ?>
		<header class="title">
			<h1>Blood Bowl</h1>	
		</header>
		<section>
			<article>
				<details open>
					<summary>Pågående Runda</summary>
					<figure>
						<iframe seamless id="runda" src='https://docs.google.com/spreadsheet/pub?key=0Apy0M0uvIhl4dDkwYjVHNE4yYTZtYUREempYNzY0aGc&single=true&gid=8&range=A1%3AF9&output=html'></iframe>
					</figure>
				</details>
			</article>
			<article>
				<details>
					<summary>In Memoriam</summary>
					<figure>
						<iframe seamless id="dod" src='https://docs.google.com/spreadsheet/pub?key=0Apy0M0uvIhl4dDkwYjVHNE4yYTZtYUREempYNzY0aGc&single=true&gid=9&range=A1%3AD11&output=html'></iframe>
					</figure>
				</details>
			</article>
			<article>
				<details>
					<summary>Ställning i Reax Cup s4</summary>
					<figure>
						<iframe seamless id="s3" src='https://docs.google.com/spreadsheet/pub?key=0Apy0M0uvIhl4dDkwYjVHNE4yYTZtYUREempYNzY0aGc&single=true&gid=2&range=A1%3AG16&output=html'></iframe>
					</figure>
				</details>
			</article>
			<article>
				<details>
					<summary>Resultat i Reax Cup s3</summary>
					<figure>
						<iframe seamless id="s3" src='https://docs.google.com/spreadsheet/pub?key=0Apy0M0uvIhl4dDkwYjVHNE4yYTZtYUREempYNzY0aGc&single=true&gid=2&range=A17%3AG31&output=html'></iframe>
					</figure>
				</details>
			</article>
			<article>
				<details>
					<summary>Resultat i Reax Cup s2</summary>
					<figure>
						<iframe seamless id="s2" src='https://docs.google.com/spreadsheet/pub?key=0Apy0M0uvIhl4dDkwYjVHNE4yYTZtYUREempYNzY0aGc&single=true&gid=2&range=A32%3AG45&output=html'></iframe>
					</figure>
				</details>
			</article>
			<article>
				<details> 
					<summary>Resultat i Reax Cup s1</summary>
					<figure>
						<iframe seamless id="s1" src='https://docs.google.com/spreadsheet/pub?key=0Apy0M0uvIhl4dDkwYjVHNE4yYTZtYUREempYNzY0aGc&single=true&gid=2&range=A46%3AG55&output=html'></iframe>
					</figure>
				</details>
			</article>
		</section>
		<p><a href="https://docs.google.com/spreadsheet/ccc?key=0Apy0M0uvIhl4dDkwYjVHNE4yYTZtYUREempYNzY0aGc&usp=sharing" title="Fullständig statestik" target="_blank">Fullständig statestik<a></p>
		
		<details>
			<summary>Blood Bowl på Steam</summary>
			<iframe src="http://store.steampowered.com/widget/216890/17129/" frameborder="0" width="646" height="190"></iframe>
			<iframe src="http://store.steampowered.com/widget/58520/6531/" frameborder="0" width="646" height="190"></iframe>
		</details>
		<?php include './include/footer'; ?>
	</body>
</html>
