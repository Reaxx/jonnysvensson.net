<!DOCTYPE html>
<html lang="sv">
	<head>
		<?php include './include/head'; ?>	
		<title>Projekt</title>
	</head>
	<body>
		<?php include './include/nav.htm'; ?>
		<header class="title">
			<h1>Projekt</h1>	
		</header>
		<table class="center">
			<thead>
				<th>Namn</th>
				<th>Beskrivning</th>
				<th>Status</th>
			</thead>
			<tbody>
				<tr>
					<td><a href="http://www.hourglass.nu/roll.html" target="_blank">DiceGen</a></td>
					<td>Tärningesgenerator</td>
					<td>Fungerande, för stunden inte under utveckling. Koden skriven av mig, men layout och bilddesign står bortom min omedelbara kontroll</td>
				</tr>
				<tr>
					<td><a href="http://www.pms.nu/spela_roll/projekt/namegen" target="_blank">NameGen</a></td>
					<td>Namngenerator</td>
					<td>Fungerande, inte under aktiv utveckling men uppdateras med nya listor vid behov</td>
				</tr>
			</tbody>
		</table>
		<?php include './include/footer'; ?>
	</body>
</html>
