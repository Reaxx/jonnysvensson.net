<!DOCTYPE html>
<html lang="sv">
	<head>
		<?php include './include/head'; ?>	
		<title>Start</title>
	</head>
	<body>
		<?php include './include/nav.htm'; ?>
		<header class="title">
			<h1>Start</h1>
		</header>
		<section class="main">
			<article>
				<p>
					Eftersom sidan framförallt skrivs på en netbook, med chrome och i stunder utan uppkoppling så är sidan tyvärr framförallt optimerad för just chrome på en relativt liten skärm. Jag kommer att efterhand gå igenom och koda upp så att sidan även stödjs ordentligt av andra webbläsare och så att layouten fungerar bra även på större skärmar.
				</p>
				<p>
					Det är även värt att påpeka att jag tenderar att arbeta direkt mot servern när jag uppdaterar sidan, och att det därmed kan krypa in en hel massa fel. De flesta borde dock ganska omedelbart upptäckas och fixas, men kan leda till att hela eller delar av sidan slutar fungera under kortare stunder.
				</p>
			</article>
		</section>
		<aside>
		<article>
					<header>
						<h4>2015-05-17</h4>
					</header>
					<p>
						Vi har nu skickat iväg dokument till Sverok för att försöka återuppliva den sedan flera år vilande föreningen Spela Roll!
					</p>
			</article>
		<article>
					<header>
						<h4>2014-03-14</h4>
					</header>
					<p>
						Säsong 3 av <a href="bb.php" title="Blood Bowl">Reax Cup</a> är nu officiellt startad!
					</p>
			</article>
		<article>
					<header>
						<h4>2014-03-05</h4>
					</header>
					<p>
						Minecraft-servern är uppdaterad till Direwolf 1.0.16, mer information finns under <a href="minecraft.php" title="Minecraft">Minecraft</a><br>
					</p>
					<header>
						<h4>2014-02-20</h4>
					</header>
					<p>
						I Blood Bowl-relaterade nyheter så är nu Reax Cup s2 avslutad, än en gång med mig som slutgiltig vinnare (om än inte längreo obesegread), en ny säsong kommer dra igång inom kort. Är folk intresserade att vara med och spela så kontakta mig snarast.
					</p>

					<header>
						<h4>2014-01-09</h4>
					</header>
					<p>
						Jag har nu startat om Minecraft-servern med nytt modpack, nu kör vi Direwolf20!.. All nödvändig info finns i <a href="minecraft.php" title="Minecraft">Minecraft</a> sidan.
					</p>
			</article>
			<article>
					<header>
						<h4>2013-11-15</h4>
					</header>
					<p>
						Minecraft-servern är återigen i drift.
					</p>
			</article>
			<article>
					<header>
						<h4>2013-11-09</h4>
					</header>
					<p>
						Minecraft-servern ligger just nu nere, håller på att försöka lista ut vad som strular och ska förhoppningsvis ha den fungerande snart igen.
					</p>
			</article>
			<article>
					<header>
						<h4>2013-11-05</h4>
					</header>
					<p>
						Kompletterade med en sida om vår Minecraft-server.
					</p>
			</article>
				<article>
					<header>
						<h4>2013-10-23</h4>
					</header>
					<p>
						Ny domän och nytt webhotell har införskaffats och jag har börjat lägga upp vad som förhoppningvis kommer bli en fungerande webbsida.
					</p>

			</article>
		</aside>
		<?php include './include/footer'; ?>
	</body>
</html>
