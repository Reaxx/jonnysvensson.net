<!DOCTYPE html>
<html lang="sv">
	<head>
		<?php include './include/head'; ?>	
		<title>Länkar</title>
	</head>
	<body>
		<?php include './include/nav.htm'; ?>
		<header class="title">
			<h1>Länkar</h1>	
		</header>
		<table>
			<thead>
				<th>Namn</th>
				<th>Beskrivning</th>
			</thead>
			<tbody>
				<tr>
					<td><a href="http://syskonskapet.se/" target="_blank">Syskonskapet</a></td>
					<td>Sammling rollspels-chattar</td>
					<td></td>
				</tr>
				<tr>
					<td><a href="http://www.syskonskapet.se/worlds/vampiri/wiki" target="_blank">Wikipiri</a></td>
					<td>Wiki för <a href="http://www.syskonskapet.se/index.php/chatt/vampiri" target="_blank">Vampiri</a> (Rollspels-chatt på <a href="http://syskonskapet.se/" target="_blank">Syskonskapet</a>)</td>
					<td></td>
				</tr>
				<tr>
					<td><a href="https://www.facebook.com/groups/123075404443413/" target="_blank">FB: Vampiri</a></td>
					<td><a href="http://www.syskonskapet.se/index.php/chatt/vampiri" target="_blank">Vampiris</a> facebook-grupp</td>
					<td></td>
				</tr>
				<tr>
					<td><a href="https://www.facebook.com/groups/593125990727914/" target="_blank">FB: Blood Bowl</a></td>
					<td>Facebook-grupp för vår <a href="bb.php" target="_blank">Blood Bowl turnering</a>, och generellt Blood Bowl pratande</td>
					<td></td>
				</tr>
			</tbody>
		</table>
		<?php include './include/footer'; ?>
	</body>
</html>
